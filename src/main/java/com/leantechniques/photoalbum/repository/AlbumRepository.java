package com.leantechniques.photoalbum.repository;

import com.leantechniques.photoalbum.model.Album;

public interface AlbumRepository {
    Album findAlbum(String albumId);
}
