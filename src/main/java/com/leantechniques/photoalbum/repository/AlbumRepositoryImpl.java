package com.leantechniques.photoalbum.repository;

import com.google.gson.Gson;
import com.leantechniques.photoalbum.model.Photo;
import com.leantechniques.photoalbum.service.AlbumService;
import com.leantechniques.photoalbum.model.Album;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class AlbumRepositoryImpl implements AlbumRepository {

    private AlbumService service;

    public AlbumRepositoryImpl(AlbumService service) {
        this.service = service;
    }

    @Override
    @Cacheable("albums")
    public Album findAlbum(String albumId) {

        String jsonStr = service.getJSONStr(albumId);

        return buildAlbum(jsonStr);
    }

    private Album buildAlbum(String albumJSON) {

        List<Photo> photos = Arrays.asList(new Gson().fromJson(albumJSON, Photo[].class));

        if(photos.isEmpty()) {
            return null;
        }

        return new Album(photos.get(0).getAlbumId(), photos);
    }

}
