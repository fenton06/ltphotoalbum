package com.leantechniques.photoalbum;

import com.leantechniques.photoalbum.repository.AlbumRepository;
import com.leantechniques.photoalbum.model.Album;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class PhotoAlbumRunner implements CommandLineRunner {

    private static final String EXIT = "exit";

    private AlbumRepository repository;

    public PhotoAlbumRunner(AlbumRepository albumRepository) {
        this.repository = albumRepository;
    }

    @Override
    public void run(String... args) {

        Scanner input = new Scanner(System.in);

        System.out.println("\nPlease enter a command:");

        while(parseLine(input.nextLine())) {
            System.out.println("Please enter a command:");
        }

        input.close();
        System.out.println("Exiting...");
    }

    protected boolean parseLine(String line) {

        line = line.toLowerCase();

        String[] tokens = line.split("\\s+");

        if(EXIT.equals(tokens[0])) {
            return false;
        }

        if(tokens.length > 2) {
            System.out.println("Too many parameters entered!");
        }

        if("photo-album".equals(tokens[0])) {

            Album album = repository.findAlbum(tokens[1]);

            if(album != null) {
                System.out.println(album.toString());
            } else {
                System.out.println("Album not found");
            }
        } else {
            System.out.println("Command not recognized");
        }

        return true;
    }

}
