package com.leantechniques.photoalbum.service;

public interface AlbumService {
    String getJSONStr(String albumId);
}
