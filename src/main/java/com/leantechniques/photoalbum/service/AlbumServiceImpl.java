package com.leantechniques.photoalbum.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.policy.TimeoutRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@PropertySource("/application.properties")
public class AlbumServiceImpl implements AlbumService {

    private static final Logger logger = LoggerFactory.getLogger(AlbumServiceImpl.class);

    @Value("${album.service.url}")
    private String url;

    private static RestTemplate rest = new RestTemplate();
    private static HttpHeaders headers = new HttpHeaders();

    public String getJSONStr(String albumId) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("albumId", albumId);

        headers.setContentType(MediaType.APPLICATION_JSON);

        RetryTemplate template = new RetryTemplate();

        TimeoutRetryPolicy policy = new TimeoutRetryPolicy();
        policy.setTimeout(30000L); // 30 seconds

        template.setRetryPolicy(policy);

        String result = null;

        try {
            result = template.execute((RetryCallback<String, Throwable>) context -> {
                HttpEntity<String> requestEntity = new HttpEntity<>(headers);
                ResponseEntity<String> responseEntity = rest.exchange(builder.toUriString(), HttpMethod.GET, requestEntity, String.class);

                return responseEntity.getBody();
            });
        } catch (Throwable throwable) {
            logger.debug(throwable.getMessage());
        }

        return result;
    }

}
