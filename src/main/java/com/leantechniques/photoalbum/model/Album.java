package com.leantechniques.photoalbum.model;

import java.util.List;

public class Album {

    private String albumId;
    private List<Photo> photos;

    public Album(String albumId, List<Photo> photos) {
        this.albumId = albumId;
        this.photos = photos;
    }

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {

        StringBuilder album = new StringBuilder();

        for(Photo photo : photos) {
            album.append(photo);
            album.append("\n\n");
        }

        return album.toString();
    }

}
