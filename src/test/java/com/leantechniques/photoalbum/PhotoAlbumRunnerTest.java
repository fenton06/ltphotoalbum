package com.leantechniques.photoalbum;

import com.leantechniques.photoalbum.model.Album;
import com.leantechniques.photoalbum.repository.AlbumRepository;
import com.leantechniques.photoalbum.repository.AlbumRepositoryImpl;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PhotoAlbumRunnerTest {

    @Test
    public void noInput() {

        AlbumRepository repository = mock(AlbumRepositoryImpl.class);
        PhotoAlbumRunner runner = new PhotoAlbumRunner(repository);

        assertTrue(runner.parseLine(""));
    }

    @Test
    public void correctInput() {

        AlbumRepository repository = mock(AlbumRepositoryImpl.class);
        Album album = mock(Album.class);
        PhotoAlbumRunner runner = new PhotoAlbumRunner(repository);

        when(repository.findAlbum("3")).thenReturn(album);

        assertTrue(runner.parseLine("photo-album 3"));
    }

    @Test
    public void tooManyArgs() {

        AlbumRepository repository = mock(AlbumRepositoryImpl.class);
        PhotoAlbumRunner runner = new PhotoAlbumRunner(repository);

        assertTrue(runner.parseLine("photo-album 2 1"));
    }

    @Test
    public void exitInput() {

        AlbumRepository repository = mock(AlbumRepositoryImpl.class);
        PhotoAlbumRunner runner = new PhotoAlbumRunner(repository);

        assertFalse(runner.parseLine("exit"));
        assertFalse(runner.parseLine("EXIT"));
        assertFalse(runner.parseLine("eXit"));
    }
}
