package com.leantechniques.photoalbum.repository;

import com.leantechniques.photoalbum.model.Album;
import com.leantechniques.photoalbum.model.Photo;
import com.leantechniques.photoalbum.service.AlbumService;
import com.leantechniques.photoalbum.service.AlbumServiceImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AlbumRepositoryImplTest {

    private static final String mockJSONStr = "[{\"albumId\": 3,\"id\": 101,\"title\": \"incidunt alias vel enim\",\"url\": \"https://via.placeholder.com/600/e743b\",\"thumbnailUrl\": \"https://via.placeholder.com/150/e743b\"},{\"albumId\": 3,\"id\": 102,\"title\": \"eaque iste corporis tempora vero distinctio consequuntur nisi nesciunt\",\"url\": \"https://via.placeholder.com/600/a393af\",\"thumbnailUrl\": \"https://via.placeholder.com/150/a393af\"}]";

    @Test
    public void findAlbumValidId() {

        AlbumService service = mock(AlbumServiceImpl.class);
        when(service.getJSONStr("3")).thenReturn(mockJSONStr);

        AlbumRepository repository = new AlbumRepositoryImpl(service);

        Album album = repository.findAlbum("3");
        assertEquals("3", album.getAlbumId());

        assertEquals(2, album.getPhotos().size());

        Photo photo1 = album.getPhotos().get(0);
        Photo photo2 = album.getPhotos().get(1);

        assertEquals("101", photo1.getId());
        assertEquals("incidunt alias vel enim", photo1.getTitle());
        assertEquals("https://via.placeholder.com/600/e743b", photo1.getUrl());
        assertEquals("https://via.placeholder.com/150/e743b", photo1.getThumbnailUrl());
        assertEquals("102", photo2.getId());
        assertEquals("eaque iste corporis tempora vero distinctio consequuntur nisi nesciunt", photo2.getTitle());
        assertEquals("https://via.placeholder.com/600/a393af", photo2.getUrl());
        assertEquals("https://via.placeholder.com/150/a393af", photo2.getThumbnailUrl());

    }

    @Test
    public void findAlbumInvalidId() {

        AlbumService service = mock(AlbumServiceImpl.class);
        when(service.getJSONStr("500")).thenReturn("[]");

        AlbumRepository repository = new AlbumRepositoryImpl(service);

        Album album = repository.findAlbum("500");

        assertNull(album);

    }

}
